public class Player {
	private char name;
	
	private int win, draw, lose;
	
	public Player(char name) {
		this.name = name;
	}
	
	public char getName( ) {
		return name;
	}
	
	public void win() {
		win++;
	}
	
	public void draw() {
		draw++;
	}
	
	public void lose() {
		lose++;
	}

	public int getWin() {
		return win;
	}

	public int getDraw() {
		return draw;
	}


	public int getLose() {
		return lose;
	}
}
